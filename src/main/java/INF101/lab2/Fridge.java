package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    private ArrayList<FridgeItem> contents;
    private int capacity;


    public Fridge() {
        contents = new ArrayList<FridgeItem>();
        capacity = 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if ((this.contents.size() + 1) > this.capacity) {
            return false;
        }
        else {
            this.contents.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (this.contents.contains(item)) {
            this.contents.remove(this.contents.indexOf(item));
        }
        else {
            throw new NoSuchElementException("Item not in fridge");
        }
    }    

    @Override
    public int nItemsInFridge() {
        return this.contents.size();
    }

    @Override
    public int totalSize() {
        return this.capacity;
    }

    @Override
    public void emptyFridge() {
        this.contents = new ArrayList<FridgeItem>();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> BadFood = new ArrayList<FridgeItem>();
        for (int i = 0; i < this.contents.size(); i++) {
            if (this.contents.get(i).hasExpired()) {
                BadFood.add(this.contents.get(i));
            }
        }
        this.contents.removeAll(BadFood);
        return BadFood;
    }
    
}
